/* Extract BIOS BIN from EXE */

#include <stdio.h>
#include <stdlib.h>

int main()
{
  FILE *f,*ff;
  size_t sz;
  int i;

  f = fopen("BIOS300.EXE","rb");
  if(f==NULL) return -1;
  ff = fopen("BIOS300.BIN","wb");
  if(ff==NULL)
  {
     fclose(f);
     return -2;
  }
  fseek(f,0,SEEK_END);
  sz = ftell(f) - 0x8000;
  fseek(f,0x8000,SEEK_SET);
  for(i=0;i<sz;i++)
  {
     fputc(fgetc(f),ff);
  }
  fclose(f);
  fclose(ff);

  return 0;
}
