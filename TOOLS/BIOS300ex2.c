/* Extract BIOS parts from BIN */

#include <stdio.h>
#include <stdlib.h>

#define BIOS "BIOS300"

int main()
{
  FILE *f,*ff,*fff,*ffff;
  size_t sz;
  int i;

  f = fopen(BIOS ".BIN","rb");
  if(f==NULL) return -1;
  ff = fopen(BIOS "exp.BIN","wb");
  if(ff==NULL)
  {
     fclose(f);
     return -2;
  }
  fff = fopen(BIOS "alt.BIN","wb");
  if(fff==NULL)
  {
     fclose(ff);
     fclose(f);
     return -3;
  }
  ffff = fopen(BIOS "set.BIN","wb");
  if(ffff==NULL)
  {
     fclose(fff);
     fclose(ff);
     fclose(f);
     return -3;
  }
  for(i=0;i<0x4000;i++)
  {
     fputc(fgetc(f),ffff);
  }
  fseek(f,0x20000,SEEK_SET);
  for(i=0;i<0x4000;i++)
  {
     fputc(fgetc(f),ff);
  }
  fseek(f,0x30100,SEEK_SET);
  for(i=0;i<59215;i++)
  {
     fputc(fgetc(f),fff);
  }
  fclose(f);
  fclose(ff);
  fclose(fff);

  return 0;
}
