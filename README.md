# Sprinter BIOS source code

This is a source code of Sprinter BIOS (that includes BIOS itself, SETUP utility and Altera bitstream)
for 8-bit computer PetersPlus Sprinter (Sp2000) that used Z80-compatible microprocessor running on 21 MHz
frequency and Altera PLD that performed all magic. To compile the code you need to use AS80.EXE
assembler for DOS (v1.31) and also CP/M emulator Z80MU with M80 and L80 utilities (not included).

Latest buildable version of source code is 2.17.252 (03-MAR-2002) and we are working on recreation
of sources for 3.00.253 (10-APR-2002), 3.03.253 (05-FEB-2003) and 3.04.253 (16-JUN-2003).

Sprinter BIOS v3.00.253 was last officially released binary distribution of Sprinter BIOS
(released by Peters Plus Ltd in April 2002), later binaries for 3.03 and 3.04 appeared from
nowhere, but the source code of earlier version 2.17 (with 253 setup) was publicly released
to community in 2009 (we believe it was done under PUBLIC DOMAIN terms).

Purpose of this particular repo is just to store officially released sources AS IS
in browsable manner for historical and educational purposes. We did some work to make
them recompilable again, but mostly it was preserved as is. Feel free to fork it if
you want to do anything with it.

For more info see Sprinter Unofficial http://sprinter.nedopc.org
